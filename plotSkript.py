#!/usr/bin/python3
import matplotlib.pyplot as plt
from scipy import *
import csv
import os
import numpy as np
import math
from sklearn.metrics import r2_score
from myVars import *
from F_Test import *

def dry(t, a):
    return Vars.omegazero - (a * t) / Vars.inertia

def stokes(t, b):
    return Vars.omegazero * np.exp((b * t) / Vars.inertia)

def newton(t, c):
    return Vars.omegazero / (1 + ((c * Vars.omegazero * t) / Vars.inertia))

def dryStokes(t, a, b):
    return (Vars.omegazero + (a / b)) * np.exp((-b * t) / Vars.inertia) - (a / b)

def dryNewton(t, a, c):
    # check if sqrt is undef. if it is return very bad values
    # t and inertia isnt importatnt since they are always > 0 
    if(a * c < 0):
        values = np.empty(t.size)
        for i in range(t.size):
            values[i] = -10000
        return values
    
    z = np.empty(t.size)
    for i in range(t.size):
        z[i] = np.tan(math.sqrt(a * c * t[i]) / Vars.inertia)
    return (Vars.omegazero - math.sqrt(a / c) * z) / (1 + Vars.omegazero * math.sqrt(c / a) * z)

def stokesNewton(t, b, c):
    return -b * Vars.omegazero / (c * Vars.omegazero - (Vars.omegazero * c + b) * np.exp((b * t) / Vars.inertia))

def dryStokesNewton(t, a, b, c):
    if((4 * a * c) - (b ** 2) < 0):
        vals = np.empty(t.size)
        for i in range(t.size):
            vals[i] = -10000
        return vals
    else:
        y = math.sqrt((4 * a * c) - (b ** 2))
        #rint("y: ",y)
        arg = (y * t) / (2 * Vars.inertia)
        #rint("arg: ",arg)
        zwc = 2 * Vars.omegazero * c
        zaehler = zwc + b - (y * np.tan(arg))
        #rint("zaehler: ", zaehler, ":", zwc + b)
        nenner = 2*c*(1+zwc*np.tan(arg)/y)
        #print("nenner: ", nenner, ":", 2 * c)
        #print("b/2c: ", b/2*c)
        #print("z/n", (zaehler/ nenner) -(b/2*c))
        return (zaehler/ nenner) -(b/(2*c))


param = {}
param_covariance = {}
errors = {}
paramWithErr = {}
r2s = {}
    
    
def plot(times, values, consider_times, consider_values, first=0, last=None, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, outputFile = None, savePngAs = None, onlyShowRegressionIntervalls = False, silentMode = False, problemChildP0 = None):

    if last == None:
        last = times.size
        
    '''
    print(times.size)
    t = times
    v = values
    for i in range(times.size):
        if values[i] < -1.0:
            t = np.delete(times, i)
            v= np.delete(values, i)

    times = t
    values = v

    print(times.size)
    '''

    #Vars.omegazero = np.max(values[:last])
    #print(Vars.omegazero)

    
    #intervalls to show datapoints
    if onlyShowRegressionIntervalls :
        show_times = times[first:last]
        show_values = values[first:last]
    else:
        show_times = times
        show_values = values
        

    # Parameter festlegen
    min = np.min(consider_times)
    max = np.max(consider_times)
    N = 10000
    
    plt.figure(figsize=(12, 6))

    # Intervall erzeugen und ausgeben
    reg_times_interval = np.linspace(min, max, N)

    # Titelierung
    #plt.title("Startwinkegeschwindigkeiz", fontsize=16)

    # Achsenbeschriftungen
    plt.xlabel("Zeit in [s]", fontsize=16)
    plt.ylabel("Winkelgeschwindigkeit in [rads/s]", fontsize=16)

    # Datenpunkte plotten
    #plt.scatter(times, values, s=1, color="#00FF7F")
    (_, caps,_) = plt.errorbar(show_times, show_values, yerr = 0.0013890255952917672, fmt='none', marker="x", markersize = 8, capsize = 2, color="#00FF7F")

    for cap in caps:
        cap.set_markeredgewidth(1)

    if showDry:
        param["dry"], param_covariance["dry"] = optimize.curve_fit(dry, consider_times, consider_values)
        reg_values_interval = dry(reg_times_interval, param["dry"][0])        
        # + times[first] compensates the offset
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "b", label="dry")
        r2s["dry"] = r2_score(consider_values, dry(consider_times, param["dry"][0]))
    if showStokes:
        param["stokes"], param_covariance["stokes"] = optimize.curve_fit(stokes, consider_times, consider_values)
        reg_values_interval = stokes(reg_times_interval, param["stokes"][0])
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "g", label="stokes")
        r2s["stokes"] = r2_score(consider_values, stokes(consider_times, param["stokes"][0]))
    if showNewton:
        param["newton"], param_covariance["newton"] = optimize.curve_fit(newton, consider_times, consider_values)
        reg_values_interval = newton(reg_times_interval, param["newton"][0])
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "r", label="newton")
        r2s["newton"] = r2_score(consider_values, newton(consider_times, param["newton"][0]))
    if showDryStokes:
        param["dryStokes"], param_covariance["dryStokes"] = optimize.curve_fit(dryStokes, consider_times, consider_values)
        reg_values_interval = dryStokes(reg_times_interval, param["dryStokes"][0], param["dryStokes"][1])
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "r", label="dryStokes")
        r2s["dryStokes"] = r2_score(consider_values, dryStokes(consider_times, param["dryStokes"][0], param["dryStokes"][1]))
    if showDryNewton:
        param["dryNewton"], param_covariance["dryNewton"] = optimize.curve_fit(dryNewton, consider_times, consider_values, p0=[1,1], maxfev= 5000)
        reg_values_interval = dryNewton(reg_times_interval, param["dryNewton"][0], param["dryNewton"][1])
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "r", label="dryNewton")
        r2s["dryNewton"] = r2_score(consider_values, dryNewton(consider_times, param["dryNewton"][0], param["dryNewton"][1]))
    if showStokesNewton:
        param["stokesNewton"], param_covariance["stokesNewton"] = optimize.curve_fit(stokesNewton, consider_times, consider_values)
        reg_values_interval = stokesNewton(reg_times_interval, param["stokesNewton"][0], param["stokesNewton"][1])
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "y", label="stokesNewton")
        r2s["stokesNewton"] = r2_score(consider_values, stokesNewton(consider_times, param["stokesNewton"][0], param["stokesNewton"][1]))
    if showDryStokesNewton:
        '''
        try:
            if problemChildP0 == None:
                param["dryStokesNewton"], param_covariance["dryStokesNewton"] = optimize.curve_fit(dryStokesNewton, consider_times, consider_values)
            else:
                param["dryStokesNewton"], param_covariance["dryStokesNewton"] = optimize.curve_fit(dryStokesNewton, consider_times, consider_values, p0 = problemChildP0)
            
            reg_values_interval = dryStokesNewton(reg_times_interval, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2])
            plt.plot(reg_times_interval + times[first], reg_values_interval, color = "b" , label="dryStokesNewton")

            r2s["dryStokesNewton"] = r2_score(consider_values, dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]))
            
        except:
            print("could not find dryStokesNewton regression")
           ''' 
        #on purpouse without error safety
        param["dryStokesNewton"], param_covariance["dryStokesNewton"] = optimize.curve_fit(dryStokesNewton, consider_times, consider_values, p0 = problemChildP0, maxfev = 5000)
        reg_values_interval = dryStokesNewton(reg_times_interval, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2])
        
        #comment out to take DSN out 
        plt.plot(reg_times_interval + times[first], reg_values_interval, color = "r" , label="dryStokesNewton")

        r2s["dryStokesNewton"] = r2_score(consider_values, dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]))
    


    #calculate erros from covariance
    for fit in param_covariance:
        errors[fit] = np.empty(param_covariance[fit][0].size)
        for i in range(param_covariance[fit][0].size):
            errors[fit][i] = np.sqrt(param_covariance[fit][i][i])

    #create parameter with error dictionary and
    if not silentMode: 
        print("results with errors and r^2")
    for fit in errors:
        if not silentMode: 
            print("     ", fit)
            print("                          r^2: ", r2s[fit]) 
        parameterArray = []
        for i in range(param_covariance[fit][0].size):
            parameterArray.append("%s ± %s"%(param[fit][i], errors[fit][i]))
            if not silentMode: 
                print("                         ",param[fit][i], " ± ",errors[fit][i])
        paramWithErr[fit] = parameterArray
        #print(fit, ": ", paramWithErr[fit])

    # Legende erzeugen
    plt.legend()

    #save as png file
    if savePngAs != None:
        plt.savefig("output/" + savePngAs)
        
    
    #actually displaying it
    plt.show()
    
    #writing results to csv file
    if outputFile != None:
        if not silentMode: 
            print(r2s)
        # writing to csv file 
        with open("output/" + outputFile, 'w') as csvfile:
            
            # creating a csv writer object 
            csvwriter = csv.writer(csvfile)

            # writing the fields
            fields = list(r2s.keys())
            fields.insert(0, "")
            csvwriter.writerow(fields)

            # writing the data rows
            #R²
            r2array = ["R²: "]
            for fit in r2s.keys():
                r2array.append(r2s[fit])
            
            aArray = ["a: ", "","","","","","",""]
            bArray = ["b: ", "","","","","","",""]
            cArray = ["c: ", "","","","","","",""]
            
            for fit in paramWithErr:
                if fit == "dry":
                    aArray[1] = paramWithErr[fit][0]
                    bArray[1] = "-"
                    cArray[1] = "-"
                    
                if fit == "stokes":
                    aArray[2] = "-"
                    bArray[2] = paramWithErr[fit][0]
                    cArray[2] = "-"
                    
                if fit == "newton":
                    aArray[3] = "-"
                    bArray[3] = "-"
                    cArray[3] = paramWithErr[fit][0]
                    
                if fit == "dryStokes":
                    aArray[4] = paramWithErr[fit][0]
                    bArray[4] = paramWithErr[fit][1]
                    cArray[4] = "-"
                    
                if fit == "dryNewton":
                    aArray[5] = paramWithErr[fit][0]
                    bArray[5] = "-"
                    cArray[5] = paramWithErr[fit][1]
                    
                if fit == "stokesNewton":
                    aArray[6] = "-"
                    bArray[6] = paramWithErr[fit][0]
                    cArray[6] = paramWithErr[fit][1]
                    
                if fit == "dryStokesNewton":
                    aArray[7] = paramWithErr[fit][0]
                    bArray[7] = paramWithErr[fit][1]
                    cArray[7] = paramWithErr[fit][2]
                    
            
            
            sse = ["SSE: ", "","","","","","",""]
            
            #calculate Sum of squared Errors
            for fit in paramWithErr:
                if fit == "dry":
                    sse[1] = SSE(consider_values, dry(consider_times, param["dry"][0]))
                    
                if fit == "stokes":
                    sse[2] = SSE(consider_values, stokes(consider_times, param["stokes"][0]))
                    
                if fit == "newton":
                    sse[3] = SSE(consider_values, newton(consider_times, param["newton"][0]))
                    
                if fit == "dryStokes":
                    sse[4] = SSE(consider_values, dryStokes(consider_times, param["dryStokes"][0], param["dryStokes"][1]))
                    
                if fit == "dryNewton":
                    sse[5] = SSE(consider_values, dryNewton(consider_times, param["dryNewton"][0], param["dryNewton"][1]))
                    
                if fit == "stokesNewton":
                    sse[6] = SSE(consider_values, stokesNewton(consider_times, param["stokesNewton"][0], param["stokesNewton"][1]))
                    
                if fit == "dryStokesNewton":
                    sse[7] = SSE(consider_values, dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]))
            
            
            #actually write the rows
            rows = [r2array, sse, aArray, bArray, cArray]
            csvwriter.writerows(rows) 
            
    
    return param, r2s




'''

def plotAndSaveBestFit(times, values, consider_times, consider_values, first, last, param, r2s):
    print(max(r2s))
    
    
    # Parameter festlegen
    minimum = np.min(consider_times)
    maximum = np.max(consider_times)
    N = 10000
    
    #intervalls to show datapoints
    if(0!=0):
        show_times = times[first:last]
        show_values = values[first:last]
    else:
        show_times = times
        show_values = values
    
    plt.figure(figsize=(12, 6))

    # Intervall erzeugen und ausgeben
    reg_times_interval = np.linspace(minimum, maximum, N)

    # Titelierung
    plt.title("Plot", fontsize=16)

    # Achsenbeschriftungen
    plt.xlabel("Zeit in [keine Ahnung was]", fontsize=10)
    plt.ylabel("rads/s", fontsize=10)

    # Datenpunkte plotten
    #plt.scatter(times, values, s=1, color="#00FF7F")
    (_, caps,_) = plt.errorbar(show_times, show_values, yerr = 0.0013890255952917672, fmt='none', marker="x", markersize = 8, capsize = 2, color="#00FF7F")

    for cap in caps:
        cap.set_markeredgewidth(1)

    
    reg_values_interval = newton(reg_times_interval, param["newton"][0])
    plt.plot(reg_times_interval + times[first], reg_values_interval, color = "r", label="newton")
    '''