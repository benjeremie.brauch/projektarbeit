#!/usr/bin/python3
import matplotlib.pyplot as plt
from scipy import optimize
import os
import numpy as np
import math
from sklearn.metrics import r2_score

from plotSkript import *
from F_Test import *


def doYourThing(inputFile, lastTime, firstTime = None, problemChildP0 = None):    

    times = np.loadtxt(inputFile, float, skiprows=1 , usecols=0, delimiter=",")
    values = np.loadtxt(inputFile, float, skiprows=1, usecols=2, delimiter=",")

    #it appears we oriented the phone the wrong way so all values are negative
    for i in range(values.size):
        values[i] = values[i] * (-1)
        
    eins = np.where(times > 0.5)[0][0]
    zwei = np.where(times > 3.5)[0][0]
    times = times[eins:zwei]
    values = values[eins:zwei]
    print(times.size)
    print(np.where(times > 0.5)[0][0])
#    print


    #trimm intervals
    last = np.where(times > lastTime)[0][0]

    if firstTime == None:
        #determine highest omegazero and beginn fit there
        Vars.omegazero = np.max(values[:last]) 
        print(Vars.omegazero)
        first = np.where(values == Vars.omegazero)[0][0]

    else:
        #begin fit at firstTime and set omega at this place as omegazero
        first = np.where(times > firstTime)[0][0]
        Vars.omegazero = np.max(values[first:last])
        print(Vars.omegazero)
        
    
    if firstTime == None:
        firstTime = 0
        
    inputFileName = inputFile.split("/", 1)[1] + " from " + str(firstTime) + " to " + str(lastTime)
        
    #create output folder if it doesnt already exists
    if not os.path.exists("./output/" + inputFileName):
        os.mkdir(os.getcwd() + "/output/" + inputFileName)
    #create output folder if it doesnt already exists
    if not os.path.exists("./output/" + inputFileName + "/croped"):
        os.mkdir(os.getcwd() + "/output/" + inputFileName + "/croped")
    #create output folder if it doesnt already exists
    if not os.path.exists("./output/" + inputFileName + "/full"):
        os.mkdir(os.getcwd() + "/output/" + inputFileName + "/full")
            


    #intervalls to consider for fit
    consider_times = times[first:last]
    consider_times = consider_times - times[first] #ofsets all timevalues by the first considered time so w0 is at t=0
    consider_values = values[first:last]
    


    param, r2s = plot(times, values, consider_times, consider_values, first, last, showDry = True, showStokes = True, showNewton = True, showDryStokes = True, showDryNewton = True, showStokesNewton = True, showDryStokesNewton = True, outputFile = inputFileName + "/results.csv", savePngAs = inputFileName + "/full/all.png")




    # full picture
    plot(times, values, consider_times, consider_values, first, last, showDry = True, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/dry.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = True, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/stokes.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = True, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/newton.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = True, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/dryStokes.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = True, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/dryNewton.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = True, showDryStokesNewton = False, savePngAs = inputFileName + "/full/" + "/stokesNewton.png", silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = True, savePngAs = inputFileName + "/full/" + "/dryStokesNewton.png", silentMode = True, problemChildP0 = problemChildP0)


    #only the part where the regression is
    plot(times, values, consider_times, consider_values, first, last, showDry = True, showStokes = True, showNewton = True, showDryStokes = True, showDryNewton = True, showStokesNewton = True, showDryStokesNewton = True, savePngAs = inputFileName + "/croped" + "/all.png", onlyShowRegressionIntervalls=True, silentMode = True, problemChildP0 = problemChildP0)
    plot(times, values, consider_times, consider_values, first, last, showDry = True, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/dry.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = True, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/stokes.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = True, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/newton.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = True, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/dryStokes.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = True, showStokesNewton = False, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/dryNewton.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = True, showDryStokesNewton = False, savePngAs = inputFileName + "/croped" + "/stokesNewton.png", onlyShowRegressionIntervalls=True, silentMode = True)
    plot(times, values, consider_times, consider_values, first, last, showDry = False, showStokes = False, showNewton = False, showDryStokes = False, showDryNewton = False, showStokesNewton = False, showDryStokesNewton = True, savePngAs = inputFileName + "/croped" + "/dryStokesNewton.png", onlyShowRegressionIntervalls=True, silentMode = True , problemChildP0 = problemChildP0)
    
    
    comparisons = ["dry and dryNewton", "dry and dryStokes", "dry and dryStokesNewton", "stokes and dryStokes", "stokes and stokesNewton", "stokes and dryStokesNewton", "newton and dryNewton", "newton and stokesNewton", "newton and dryStokesNewton"]
    
    carryOutFTests(consider_times, consider_values, param, comparisons, outputFile = inputFileName + "/F_TestResults.csv")        