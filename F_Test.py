from plotSkript import *


def SSE(dataValues, fittedValues):
    return np.sum((dataValues - fittedValues)**2)
    
def calculateFValue(times, values, fitValues1, fitValues2, p1, p2):
    SSE1 = SSE(values, fitValues1)
    SSE2 = SSE(values, fitValues2)
    n = times.size
    F = ((SSE1-SSE2)/(p2-p1)) / (SSE2/(n-p2+1))
    return F

def passesFTest(times, values, fitValues1, fitValues2, p1, p2):
    F = calculateFValue(times, values, fitValues1, fitValues2, p1, p2)
    #print("fVal: ",F)
    n = times.size
    dfn = p2-p1     #degree of freedom in numerator
    dfd = n-p2+1    #degree of freedom in denominator
    criticalValue = stats.f.ppf(q=1-0.05, dfn=dfn, dfd=dfd)
    #print("criticalVal: ",criticalValue)
    passesIt = False
    if F > criticalValue:
        passesIt = True
    #print("ftest is passed: ", passesIt)
    return passesIt

def carryOutFTests(consider_times, consider_values, param, comparisons, outputFile = None):
    
    passesTest = {}

    for comparison in comparisons:
        match comparison:
            #dry and nested models
            case "dry and dryNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = dry(consider_times, param["dry"][0]), fitValues2 = dryNewton(consider_times, param["dryNewton"][0], param["dryNewton"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "dry and dryStokes":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = dry(consider_times, param["dry"][0]), fitValues2 = dryStokes(consider_times, param["dryStokes"][0], param["dryStokes"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "dry and dryStokesNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = dry(consider_times, param["dry"][0]), fitValues2 = dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]), p1 = 1, p2 = 3 )
                print(comparison, " passes F test: ", passesTest[comparison])

            #stokes and nested models
            case "stokes and dryStokes":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = stokes(consider_times, param["stokes"][0]), fitValues2 = dryStokes(consider_times, param["dryStokes"][0], param["dryStokes"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "stokes and stokesNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = stokes(consider_times, param["stokes"][0]), fitValues2 = stokesNewton(consider_times, param["stokesNewton"][0], param["stokesNewton"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "stokes and dryStokesNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = stokes(consider_times, param["stokes"][0]), fitValues2 = dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]), p1 = 1, p2 = 3 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            #newton and nested models
            case "newton and dryNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = newton(consider_times, param["newton"][0]), fitValues2 = dryNewton(consider_times, param["dryNewton"][0], param["dryNewton"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "newton and stokesNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = newton(consider_times, param["newton"][0]), fitValues2 = stokesNewton(consider_times, param["stokesNewton"][0], param["stokesNewton"][1]), p1 = 1, p2 = 2 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            case "newton and dryStokesNewton":
                passesTest[comparison] = passesFTest(times = consider_times, values = consider_values, fitValues1 = newton(consider_times, param["newton"][0]), fitValues2 = dryStokesNewton(consider_times, param["dryStokesNewton"][0], param["dryStokesNewton"][1], param["dryStokesNewton"][2]), p1 = 1, p2 = 3 )
                print(comparison, " passes F test: ", passesTest[comparison])
                
            
    
    
    #writing results to csv file
    if outputFile != None:
        # writing to csv file 
        with open("output/" + outputFile, 'w') as csvfile:
            
            # creating a csv writer object 
            csvwriter = csv.writer(csvfile)

            # writing the fields
            fields = comparisons
            csvwriter.writerow(fields)

            # writing the data rows
            row = []
            
            for comparison in passesTest:
                row.append(passesTest[comparison])
            
            rows = [row]
            
            csvwriter.writerows(rows) 