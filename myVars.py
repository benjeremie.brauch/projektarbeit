class Vars:
    mass = 48
    width = 0.98
    inertia = (1/3) * mass * (width ** 2)
    omegazero = None